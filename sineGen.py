import struct
import numpy

sampling_rate = 44100
freq = 440
samples = 44100
x = numpy.arange(samples)

y = 100*numpy.sin(2 * numpy.pi * freq * x / sampling_rate)

f = open('sineTest.wav', 'wb')

for i in y:
	f.write(struct.pack('b', int(i)))
f.close()
