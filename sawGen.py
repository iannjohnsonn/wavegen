import struct
import numpy
from scipy import signal

sampling_rate = 44100
freq = 440
samples = 44100
x = numpy.arange(samples)

y = 100 * signal.sawtooth(2 * numpy.pi * freq * x / sampling_rate)

f = open('sawTest.wav', 'wb')

for i in y:
    f.write(struct.pack('b', int(i)))
f.close()